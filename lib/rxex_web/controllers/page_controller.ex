defmodule RxExWeb.PageController do
  use RxExWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
