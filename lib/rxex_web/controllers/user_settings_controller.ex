defmodule RxExWeb.UserSettingsController do
  use RxExWeb, :controller

  alias RxEx.Accounts
  alias RxExWeb.UserAuth

  plug :assign_email_and_password_changesets

  def edit(conn, _params) do
    render(conn, "edit.html")
  end

  def update(conn, %{"action" => "update_email"} = params) do
    %{"current_password" => password, "user" => user_params} = params
    user = conn.assigns.current_user

    case Accounts.apply_user_email(user, password, user_params) do
      {:ok, applied_user} ->
        Accounts.deliver_update_email_instructions(
          applied_user,
          user.email,
          &Routes.user_settings_url(conn, :confirm_email, &1)
        )

        conn
        |> put_flash(
          :info,
          "A link to confirm your email change has been sent to the new address."
        )
        |> redirect(to: Routes.user_settings_path(conn, :edit))

      {:error, changeset} ->
        render(conn, "edit.html", email_changeset: changeset)
    end
  end

  def update(conn, %{"action" => "update_password"} = params) do
    %{"current_password" => password, "user" => user_params} = params
    user = conn.assigns.current_user

    case Accounts.update_user_password(user, password, user_params) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "Password updated successfully.")
        |> put_session(:user_return_to, Routes.user_settings_path(conn, :edit))
        |> UserAuth.log_in_user(user)

      {:error, changeset} ->
        render(conn, "edit.html", password_changeset: changeset)
    end
  end

  def confirm_email(conn, %{"token" => token}) do
    case Accounts.update_user_email(conn.assigns.current_user, token) do
      :ok ->
        conn
        |> put_flash(:info, "Email changed successfully.")
        |> redirect(to: Routes.user_settings_path(conn, :edit))

      :error ->
        conn
        |> put_flash(:error, "Email change link is invalid or it has expired.")
        |> redirect(to: Routes.user_settings_path(conn, :edit))
    end
  end

  @doc """
  After a user is created, send them to a select page to select their pharmacy if they are a pharmacist or a courier
  service if they are a courier.
  """
  def select_pharmacy_or_courier(conn, _params) do
    user = conn.assigns.current_user
    available_options = case user.role do
      "pharmacist" -> RxEx.Address.list_pharmacies()
                      |> Enum.map(fn (x) -> [key: x.name, value: x.id] end)
      "courier" -> RxEx.Address.list_couriers()
                   |> Enum.map(fn (x) -> [key: x.name, value: x.id] end)
    end

    # Create both changesets even though only 1 is used
    conn
    |> assign(:pharmacy_changeset, Accounts.change_user_pharmacy(user))
    |> assign(:courier_changeset, Accounts.change_user_courier(user))
    # dynamically figure out the select page based on the user role
    |> render("select_#{user.role}.html", options: available_options)
  end

  @doc """
  Set the pharmacy for a user
  """
  def update_pharmacy(conn, params) do
    user = conn.assigns.current_user
    case Accounts.update_user_pharmacy(user, params) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "Pharmacy updated successfully")
        |> redirect(to: "/orders")
      :error ->
        conn
        |> put_flash(:error, "Error updating the pharmacy.")
        |> redirect(to: Routes.user_settings_path(conn, :select_pharmacy_or_courier))
    end
  end

  @doc """
  Set the courier for a user
  """
  def update_courier(conn, params) do
    user = conn.assigns.current_user
    case Accounts.update_user_courier(user, params) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "Courier updated successfully")
        |> redirect(to: "/orders")
      :error ->
        conn
        |> put_flash(:error, "Error updating the courier.")
        |> redirect(to: Routes.user_settings_path(conn, :select_pharmacy_or_courier))
    end
  end

  defp assign_email_and_password_changesets(conn, _opts) do
    user = conn.assigns.current_user

    conn
    |> assign(:email_changeset, Accounts.change_user_email(user))
    |> assign(:password_changeset, Accounts.change_user_password(user))
  end
end
