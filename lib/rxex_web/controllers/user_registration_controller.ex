defmodule RxExWeb.UserRegistrationController do
  use RxExWeb, :controller

  alias RxEx.Accounts
  alias RxEx.Accounts.User
  alias RxExWeb.UserAuth

  def new(conn, _params) do
    changeset = Accounts.change_user_registration(%User{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    case register_type(user_params) do
      {:ok, user} ->
        {:ok, _} =
          Accounts.deliver_user_confirmation_instructions(
            user,
            &Routes.user_confirmation_url(conn, :confirm, &1)
          )

        conn
        |> put_flash(:info, "User created successfully.")
        |> put_session(:user_return_to, "/users/settings/selection")
        |> UserAuth.log_in_user(user)

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  defp register_type(%{"role" => "pharmacist"} = user_params) do
    Accounts.register_pharmacist(user_params)
  end

  defp register_type(%{"role" => "courier"} = user_params) do
    Accounts.register_courier(user_params)
  end

end
