defmodule RxEx.Address.Courier do
  use Ecto.Schema
  import Ecto.Changeset


  alias RxEx.Accounts.User
  alias RxEx.Address.Pharmacy

  schema "couriers" do
    field :address, :string
    field :city, :string
    field :name, :string
    field :state, :string
    field :zip, :string

    has_many :pharmacies, Pharmacy
    has_many :users, User

    timestamps()
  end

  @doc false
  def changeset(courier, attrs) do
    courier
    |> cast(attrs, [:name, :address, :city, :state, :zip])
    |> validate_required([:name, :address, :city, :state, :zip])
  end
end
