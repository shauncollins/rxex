defmodule RxEx.Address.Pharmacy do
  use Ecto.Schema
  import Ecto.Changeset

  alias RxEx.Accounts.User
  alias RxEx.Address.Courier
  alias RxEx.Orders.Order

  schema "pharmacies" do
    field :address, :string
    field :city, :string
    field :name, :string
    field :state, :string
    field :zip, :string

    belongs_to :courier, Courier
    has_many :users, User
    has_many :orders, Order

    timestamps()
  end

  @doc false
  def changeset(pharmacy, attrs) do
    pharmacy
    |> cast(attrs, [:name, :address, :city, :state, :zip, :courier_id])
    |> validate_required([:name, :address, :city, :state, :zip, :courier_id])
  end
end
