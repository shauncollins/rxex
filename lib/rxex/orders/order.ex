defmodule RxEx.Orders.Order do
  use Ecto.Schema
  import Ecto.Changeset

  alias RxEx.Address.Pharmacy

  schema "orders" do
    field :address, :string
    field :city, :string
    field :name, :string
    field :pickup_date, :naive_datetime
    field :state, :string
    field :zip, :string
    field :delivered, :boolean
    field :delivery_date, :naive_datetime

    belongs_to :pharmacy, Pharmacy

    timestamps()
  end

  @doc false
  def changeset(order, attrs) do
    order
    |> cast(attrs, [:name, :address, :city, :state, :zip, :pickup_date, :pharmacy_id, :delivered, :delivery_date])
    |> validate_required([:name, :address, :city, :state, :zip, :pickup_date, :pharmacy_id])
  end

  def deliver_changeset(order) do
    order
    |> changeset(%{
      delivered: true,
      delivery_date: NaiveDateTime.local_now()
    })
  end
end
