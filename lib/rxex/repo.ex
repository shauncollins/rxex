defmodule RxEx.Repo do
  use Ecto.Repo,
    otp_app: :rxex,
    adapter: Ecto.Adapters.Postgres
end
