defmodule RxEx.Orders do
  @moduledoc """
  The Orders context.
  """

  import Ecto.Query, warn: false
  alias RxEx.Repo

  alias RxEx.Orders.Order
  alias RxEx.Accounts.User
  alias RxEx.Address.Pharmacy

  @doc """
  Returns the list of orders a user has access to see.  This is different for couriers and pharmacies.
  """
  def list_orders(%User{} = user) do
    today = NaiveDateTime.local_now()
            |> NaiveDateTime.to_date
            |> NaiveDateTime.new!(~T[00:00:00.000])


    tomorrow = NaiveDateTime.local_now()
               |> NaiveDateTime.add(24 * 60 * 60, :second)
               |> NaiveDateTime.to_date
               |> NaiveDateTime.new!(~T[00:00:00.000])

    query_for_orders(user, today, tomorrow)
    |> Repo.all()

  end

  defp query_for_orders(%User{role: "pharmacist", pharmacy_id: pharmacy_id}, today, tomorrow) do
    from o in Order,
         where: o.pharmacy_id == ^pharmacy_id and o.pickup_date > ^today and o.pickup_date < ^tomorrow,
         order_by: :pickup_date
  end

  # Couriers need to join the pharmacy so we can link by pharmacy id and also preload the pharmacy name for the UI
  defp query_for_orders(%User{role: "courier", courier_id: courier_id}, today, tomorrow) do
    from o in Order,
         join: p in Pharmacy,
         on: p.id == o.pharmacy_id,
         where: p.courier_id == ^courier_id and o.pickup_date > ^today and o.pickup_date < ^tomorrow,
         order_by: :pickup_date,
         preload: [:pharmacy]
  end

  @doc """
  Gets a single order.

  Raises `Ecto.NoResultsError` if the Order does not exist.

  ## Examples

      iex> get_order!(123)
      %Order{}

      iex> get_order!(456)
      ** (Ecto.NoResultsError)

  """
  def get_order!(id), do: Repo.get!(Order, id)

  @doc """
  Creates a order.

  ## Examples

      iex> create_order(%{field: value})
      {:ok, %Order{}}

      iex> create_order(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_order(attrs \\ %{}) do
    %Order{}
    |> Order.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a order.

  ## Examples

      iex> update_order(order, %{field: new_value})
      {:ok, %Order{}}

      iex> update_order(order, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_order(%Order{} = order, attrs) do
    order
    |> Order.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Mark an order as delivered
  """
  def mark_delivered(%Order{} = order) do
    order
    |> Order.deliver_changeset()
    |> Repo.update()
  end

  @doc """
  Deletes a order.

  ## Examples

      iex> delete_order(order)
      {:ok, %Order{}}

      iex> delete_order(order)
      {:error, %Ecto.Changeset{}}

  """
  def delete_order(%Order{} = order) do
    Repo.delete(order)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking order changes.

  ## Examples

      iex> change_order(order)
      %Ecto.Changeset{data: %Order{}}

  """
  def change_order(%Order{} = order, attrs \\ %{}) do
    Order.changeset(order, attrs)
  end
end
