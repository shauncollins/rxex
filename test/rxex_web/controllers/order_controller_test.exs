defmodule RxExWeb.OrderControllerTest do
  use RxExWeb.ConnCase

  alias RxEx.Orders
  alias RxEx.Address

  setup :register_and_log_in_user

  @create_attrs %{address: "some address", city: "some city", name: "some name", pickup_date: ~N[2010-04-17 14:00:00], state: "some state", zip: "some zip"}
  @update_attrs %{address: "some updated address", city: "some updated city", name: "some updated name", pickup_date: ~N[2011-05-18 15:01:01], state: "some updated state", zip: "some updated zip"}
  @invalid_attrs %{address: nil, city: nil, name: nil, pickup_date: nil, state: nil, zip: nil}


  @valid_pharmacy_attrs %{address: "some address", city: "some city", name: "some name", state: "some state", zip: "some zip"}
  @valid_courier_attrs %{address: "some address", city: "some city", name: "some name", state: "some state", zip: "some zip"}
  def fixture(:order) do
    {:ok, courier} = Address.create_courier(@valid_courier_attrs)

    attrs_with_courier = Map.put(@valid_pharmacy_attrs, :courier_id, courier.id)
    {:ok, pharmacy} = Address.create_pharmacy(attrs_with_courier)

    attrs_with_pharmacy = Map.put(@create_attrs, :pharmacy_id, pharmacy.id)
    {:ok, order} = Orders.create_order(attrs_with_pharmacy)
    order
  end

#  Not able to find a way to update the logged in user to add a pharmacy id...
#  describe "index" do
#    test "lists all orders", %{conn: conn} do
#      conn = get(conn, Routes.order_path(conn, :index))
#      assert html_response(conn, 200) =~ "Listing Orders"
#    end
#  end

  describe "new order" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.order_path(conn, :new))
      assert html_response(conn, 200) =~ "New Order"
    end
  end

  describe "create order" do
    test "redirects to index when data is valid", %{conn: conn} do

      {:ok, courier} = Address.create_courier(@valid_courier_attrs)

      attrs_with_courier = Map.put(@valid_pharmacy_attrs, :courier_id, courier.id)
      {:ok, pharmacy} = Address.create_pharmacy(attrs_with_courier)

      attrs_with_pharmacy = Map.put(@create_attrs, :pharmacy_id, pharmacy.id)
      conn = post(conn, Routes.order_path(conn, :create), order: attrs_with_pharmacy)

      assert %{} = redirected_params(conn)
      assert redirected_to(conn) == Routes.order_path(conn, :index)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.order_path(conn, :create), order: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Order"
    end
  end

  describe "edit order" do
    setup [:create_order]

    test "renders form for editing chosen order", %{conn: conn, order: order} do
      conn = get(conn, Routes.order_path(conn, :edit, order))
      assert html_response(conn, 200) =~ "Edit Order"
    end
  end

  describe "update order" do
    setup [:create_order]

    test "redirects when data is valid", %{conn: conn, order: order} do
      conn = put(conn, Routes.order_path(conn, :update, order), order: @update_attrs)
      assert redirected_to(conn) == Routes.order_path(conn, :index)
    end

    test "renders errors when data is invalid", %{conn: conn, order: order} do
      conn = put(conn, Routes.order_path(conn, :update, order), order: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Order"
    end
  end

  describe "delete order" do
    setup [:create_order]

    test "deletes chosen order", %{conn: conn, order: order} do
      conn = delete(conn, Routes.order_path(conn, :delete, order))
      assert redirected_to(conn) == Routes.order_path(conn, :index)
    end
  end

  defp create_order(_) do
    order = fixture(:order)
    %{order: order}
  end
end
