defmodule RxEx.AddressTest do
  use RxEx.DataCase

  alias RxEx.Address

  describe "pharmacies" do
    alias RxEx.Address.Pharmacy
    alias RxEx.Address.Courier

    @valid_attrs %{address: "some address", city: "some city", name: "some name", state: "some state", zip: "some zip"}
    @update_attrs %{address: "some updated address", city: "some updated city", name: "some updated name", state: "some updated state", zip: "some updated zip"}
    @invalid_attrs %{address: nil, city: nil, name: nil, state: nil, zip: nil}

    def pharmacy_fixture(attrs \\ %{}) do
      {:ok, pharmacy} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Address.create_pharmacy()

      pharmacy
    end

    test "list_pharmacies/0 returns all pharmacies" do
      courier = courier_fixture()
      pharmacy = pharmacy_fixture(%{courier_id: courier.id})
      assert Address.list_pharmacies() == [pharmacy]
    end

    test "get_pharmacy!/1 returns the pharmacy with given id" do
      courier = courier_fixture()
      pharmacy = pharmacy_fixture(%{courier_id: courier.id})
      assert Address.get_pharmacy!(pharmacy.id) == pharmacy
    end

    test "create_pharmacy/1 with valid data creates a pharmacy" do
      courier = courier_fixture()

      assert {:ok, %Pharmacy{} = pharmacy} = Address.create_pharmacy(Map.put(@valid_attrs,:courier_id, courier.id))
      assert pharmacy.address == "some address"
      assert pharmacy.city == "some city"
      assert pharmacy.name == "some name"
      assert pharmacy.state == "some state"
      assert pharmacy.zip == "some zip"
    end

    test "create_pharmacy/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Address.create_pharmacy(@invalid_attrs)
    end

    test "update_pharmacy/2 with valid data updates the pharmacy" do
      courier = courier_fixture()
      pharmacy = pharmacy_fixture(%{courier_id: courier.id})
      assert {:ok, %Pharmacy{} = pharmacy} = Address.update_pharmacy(pharmacy, @update_attrs)
      assert pharmacy.address == "some updated address"
      assert pharmacy.city == "some updated city"
      assert pharmacy.name == "some updated name"
      assert pharmacy.state == "some updated state"
      assert pharmacy.zip == "some updated zip"
    end

    test "update_pharmacy/2 with invalid data returns error changeset" do
      courier = courier_fixture()
      pharmacy = pharmacy_fixture(%{courier_id: courier.id})
      assert {:error, %Ecto.Changeset{}} = Address.update_pharmacy(pharmacy, @invalid_attrs)
      assert pharmacy == Address.get_pharmacy!(pharmacy.id)
    end

    test "change_pharmacy/1 returns a pharmacy changeset" do
      courier = courier_fixture()
      pharmacy = pharmacy_fixture(%{courier_id: courier.id})
      assert %Ecto.Changeset{} = Address.change_pharmacy(pharmacy)
    end
  end

  describe "couriers" do
    alias RxEx.Address.Courier

    @valid_attrs %{address: "some address", city: "some city", name: "some name", state: "some state", zip: "some zip"}
    @update_attrs %{address: "some updated address", city: "some updated city", name: "some updated name", state: "some updated state", zip: "some updated zip"}
    @invalid_attrs %{address: nil, city: nil, name: nil, state: nil, zip: nil, courier_id: nil}

    test "list_couriers/0 returns all couriers" do
      courier = courier_fixture()
      assert Address.list_couriers() == [courier]
    end

    test "get_courier!/1 returns the courier with given id" do
      courier = courier_fixture()
      assert Address.get_courier!(courier.id) == courier
    end

    test "create_courier/1 with valid data creates a courier" do
      assert {:ok, %Courier{} = courier} = Address.create_courier(@valid_attrs)
      assert courier.address == "some address"
      assert courier.city == "some city"
      assert courier.name == "some name"
      assert courier.state == "some state"
      assert courier.zip == "some zip"
    end

    test "create_courier/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Address.create_courier(@invalid_attrs)
    end

    test "update_courier/2 with valid data updates the courier" do
      courier = courier_fixture()
      assert {:ok, %Courier{} = courier} = Address.update_courier(courier, @update_attrs)
      assert courier.address == "some updated address"
      assert courier.city == "some updated city"
      assert courier.name == "some updated name"
      assert courier.state == "some updated state"
      assert courier.zip == "some updated zip"
    end

    test "update_courier/2 with invalid data returns error changeset" do
      courier = courier_fixture()
      assert {:error, %Ecto.Changeset{}} = Address.update_courier(courier, @invalid_attrs)
      assert courier == Address.get_courier!(courier.id)
    end

    test "change_courier/1 returns a courier changeset" do
      courier = courier_fixture()
      assert %Ecto.Changeset{} = Address.change_courier(courier)
    end
  end

  def courier_fixture(attrs \\ %{}) do
    {:ok, courier} =
      attrs
      |> Enum.into(@valid_attrs)
      |> Address.create_courier()

    courier
  end
end
