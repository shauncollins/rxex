defmodule RxEx.OrdersTest do
  use RxEx.DataCase

  alias RxEx.Orders
  alias RxEx.Address
  alias RxEx.Accounts.User

  describe "orders" do
    alias RxEx.Orders.Order

    @valid_attrs %{address: "some address", city: "some city", name: "some name", pickup_date: NaiveDateTime.local_now(), state: "some state", zip: "some zip", delivered: false}
    @update_attrs %{address: "some updated address", city: "some updated city", name: "some updated name", pickup_date: NaiveDateTime.local_now(), state: "some updated state", zip: "some updated zip", delivered: false}
    @invalid_attrs %{address: nil, city: nil, name: nil, pickup_date: nil, state: nil, zip: nil}

    @valid_pharmacy_attrs %{address: "some address", city: "some city", name: "some name", state: "some state", zip: "some zip"}
    @valid_courier_attrs %{address: "some address", city: "some city", name: "some name", state: "some state", zip: "some zip"}

    def order_fixture(attrs \\ %{}) do

      {:ok, courier} = Address.create_courier(@valid_courier_attrs)

      attrs_with_courier = Map.put(@valid_pharmacy_attrs, :courier_id, courier.id)
      {:ok, pharmacy} = Address.create_pharmacy(attrs_with_courier)

      {:ok, order} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Map.put(:pharmacy_id, pharmacy.id)
        |> Orders.create_order()

      order
    end

    test "list_orders/1 returns all orders for a users pharmacy" do
      order = order_fixture()

      # setting the pharmacy to nil in both the expected and actual result because ecto.load is different but the data matches
      order = Map.put(order, :pharmacy, nil)
      orders = Orders.list_orders(%User{role: "pharmacist", pharmacy_id: order.pharmacy_id})
        |> Enum.map( fn(x) -> Map.put(x, :pharmacy, nil) end)

      assert orders == [order]
    end

    test "get_order!/1 returns the order with given id" do
      order = order_fixture()
      assert Orders.get_order!(order.id) == order
    end

    test "create_order/1 with valid data creates a order" do

      {:ok, courier} = Address.create_courier(@valid_courier_attrs)

      attrs_with_courier = Map.put(@valid_pharmacy_attrs, :courier_id, courier.id)
      {:ok, pharmacy} = Address.create_pharmacy(attrs_with_courier)
      assert {:ok, %Order{} = order} = Orders.create_order(Map.put(@valid_attrs, :pharmacy_id, pharmacy.id))
      assert order.address == "some address"
      assert order.city == "some city"
      assert order.name == "some name"
      assert order.state == "some state"
      assert order.zip == "some zip"
    end

    test "create_order/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Orders.create_order(@invalid_attrs)
    end

    test "update_order/2 with valid data updates the order" do
      order = order_fixture()
      assert {:ok, %Order{} = order} = Orders.update_order(order, @update_attrs)
      assert order.address == "some updated address"
      assert order.city == "some updated city"
      assert order.name == "some updated name"
      assert order.state == "some updated state"
      assert order.zip == "some updated zip"
    end

    test "update_order/2 with invalid data returns error changeset" do
      order = order_fixture()
      assert {:error, %Ecto.Changeset{}} = Orders.update_order(order, @invalid_attrs)
      assert order == Orders.get_order!(order.id)
    end

    test "delete_order/1 deletes the order" do
      order = order_fixture()
      assert {:ok, %Order{}} = Orders.delete_order(order)
      assert_raise Ecto.NoResultsError, fn -> Orders.get_order!(order.id) end
    end

    test "change_order/1 returns a order changeset" do
      order = order_fixture()
      assert %Ecto.Changeset{} = Orders.change_order(order)
    end
  end
end
