defmodule RxEx.Repo.Migrations.AddPharmacyToOrder do
  use Ecto.Migration

  def change do
    alter table (:orders) do
      add :pharmacy_id, references(:pharmacies)
    end
  end
end
