defmodule RxEx.Repo.Migrations.AddDeliveryDateToOrder do
  use Ecto.Migration

  def change do
    alter table (:orders) do
      add :delivered, :boolean, default: false
      add :delivery_date, :naive_datetime
    end
  end
end
