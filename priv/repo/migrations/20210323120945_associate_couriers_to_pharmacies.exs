defmodule RxEx.Repo.Migrations.AssociateCouriersToPharmacies do
  use Ecto.Migration

  def change do
    alter table(:pharmacies) do
      add :courier_id, references(:couriers)
    end
  end
end
