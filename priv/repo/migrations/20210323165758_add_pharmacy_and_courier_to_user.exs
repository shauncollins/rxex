defmodule RxEx.Repo.Migrations.AddPharmacyAndCourierToUser do
  use Ecto.Migration

  def change do
    alter table(:users) do
      # A user will only have 1 or the other set
      add :pharmacy_id, references(:pharmacies)
      add :courier_id, references(:couriers)
    end
  end
end
