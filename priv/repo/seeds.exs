# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     RxEx.Repo.insert!(%RxEx.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias RxEx.Address

{:ok, same_day_delivery} = Address.create_courier(
  %{
    name: "Same Day Delivery",
    address: "900 Trenton Lane",
    city: "Trenton",
    state: "NJ",
    zip: "08536"
  }
)

{:ok, previous_day_delivery} = Address.create_courier(
  %{
    name: "Previous Day Delivery",
    address: "7433 LA Ct",
    city: "Los Angeles",
    state: "CA",
    zip: "90056"
  }
)

{:ok, better_rx} = Address.create_pharmacy(
  %{
    name: "BetterRx",
    address: "1275 Kinnear Road",
    city: "Columbus",
    state: "OH",
    zip: "43212",
    courier_id: same_day_delivery.id
  }
)

{:ok, best_rx} = Address.create_pharmacy(
  %{
    name: "BestRx",
    address: "123 Austin St",
    city: "Austin",
    state: "TX",
    zip: "78702",
    courier_id: same_day_delivery.id
  }
)

{:ok, drugs_r_us} = Address.create_pharmacy(
  %{
    name: "Drugs R Us",
    address: "4925 LA Ave",
    city: "Los Angeles",
    state: "CA",
    zip: "90056",
    courier_id: previous_day_delivery.id
  }
)

## Pharmacists

alias RxEx.Accounts

{:ok, better_rx_phar} = Accounts.register_pharmacist(
  %{
    email: "pharma@betterrx.com",
    password: "password12345",
    role: "pharmacist"
  }
)

{:ok, better_rx_phar} = Accounts.update_user_pharmacy(better_rx_phar, %{
  pharmacy_id: better_rx.id
})

{:ok, best_rx_phar} = Accounts.register_pharmacist(
  %{
    email: "pharma@bestrx.com",
    password: "password12345",
    role: "pharmacist"
  }
)

{:ok, better_rx_phar} = Accounts.update_user_pharmacy(best_rx_phar, %{
  pharmacy_id: best_rx.id
})

{:ok, drugs_r_us_phar} = Accounts.register_pharmacist(
  %{
    email: "pharma@drugsrus.com",
    password: "password12345",
    role: "pharmacist"
  }
)

{:ok, better_rx_phar} = Accounts.update_user_pharmacy(drugs_r_us_phar, %{
  pharmacy_id: drugs_r_us_phar.id
})

## Couriers
{:ok, same_day_delivery_courier} = Accounts.register_courier(
  %{
    email: "courier@samedaydelivery.com",
    password: "password12345",
    role: "courier"
  }
)

{:ok, same_day_delivery_courier} = Accounts.update_user_courier(same_day_delivery_courier, %{
  courier_id: same_day_delivery.id
})

{:ok, previous_day_delivery_courier} = Accounts.register_courier(
  %{
    email: "courier@previousdaydelivery.com",
    password: "password12345",
    role: "courier"
  }
)

{:ok, previous_day_delivery_courier} = Accounts.update_user_courier(previous_day_delivery_courier, %{
  courier_id: previous_day_delivery.id
})

# Orders
alias RxEx.Orders

today = NaiveDateTime.local_now()
yesterday = NaiveDateTime.add(today, -24 * 60 * 60, :second)
tomorrow = NaiveDateTime.add(today, 24 * 60 * 60, :second)

# Yesterday
#Better Rx Pharmacy
Orders.create_order(
  %{
    address: "1 Main St",
    city: "New York City",
    name: "Ted Aenny",
    pickup_date: NaiveDateTime.add(yesterday, -3 * 60 * 60, :second),
    state: "NY",
    zip: "11011",
    pharmacy_id: better_rx.id
  }
)

Orders.create_order(
  %{
    address: "103 Main St",
    city: "Brooklyn",
    name: "Ted Benny",
    pickup_date: NaiveDateTime.add(yesterday, -1 * 60 * 60, :second),
    state: "NY",
    zip: "11045",
    pharmacy_id: better_rx.id
  }
)

Orders.create_order(
  %{
    address: "145 Main St",
    city: "Brooklyn",
    name: "Ted Cenny",
    pickup_date: yesterday,
    state: "NY",
    zip: "11045",
    pharmacy_id: better_rx.id
  }
)

Orders.create_order(
  %{
    address: "199 Main St",
    city: "New York City",
    name: "Ted Denny",
    pickup_date: NaiveDateTime.add(yesterday, 1 * 60 * 60, :second),
    state: "NY",
    zip: "11011",
    pharmacy_id: better_rx.id
  }
)

Orders.create_order(
  %{
    address: "204 Main St",
    city: "Brooklyn",
    name: "Ted Eenny",
    pickup_date: NaiveDateTime.add(yesterday, 3 * 60 * 60, :second),
    state: "NY",
    zip: "11045",
    pharmacy_id: better_rx.id
  }
)

#Best Rx Pharmacy
Orders.create_order(
  %{
    address: "222 Main St",
    city: "New York City",
    name: "Ted Fenny",
    pickup_date: NaiveDateTime.add(yesterday, -3 * 60 * 60, :second),
    state: "NY",
    zip: "11011",
    pharmacy_id: best_rx_phar.id
  }
)

Orders.create_order(
  %{
    address: "245 Main St",
    city: "Brooklyn",
    name: "Ted Genny",
    pickup_date: NaiveDateTime.add(yesterday, -1 * 60 * 60, :second),
    state: "NY",
    zip: "11045",
    pharmacy_id: best_rx_phar.id
  }
)

Orders.create_order(
  %{
    address: "277 Main St",
    city: "Brooklyn",
    name: "Ted Henny",
    pickup_date: yesterday,
    state: "NY",
    zip: "11045",
    pharmacy_id: best_rx_phar.id
  }
)

Orders.create_order(
  %{
    address: "299 Main St",
    city: "New York City",
    name: "Ted Ienny",
    pickup_date: NaiveDateTime.add(yesterday, 1 * 60 * 60, :second),
    state: "NY",
    zip: "11011",
    pharmacy_id: best_rx_phar.id
  }
)

Orders.create_order(
  %{
    address: "301 Main St",
    city: "Brooklyn",
    name: "Ted Jenny",
    pickup_date: NaiveDateTime.add(yesterday, 3 * 60 * 60, :second),
    state: "NY",
    zip: "11045",
    pharmacy_id: best_rx_phar.id
  }
)

#Drugs R Us Pharmacy
Orders.create_order(
  %{
    address: "399 Main St",
    city: "New York City",
    name: "Ted Kenny",
    pickup_date: NaiveDateTime.add(yesterday, -3 * 60 * 60, :second),
    state: "NY",
    zip: "11011",
    pharmacy_id: drugs_r_us_phar.id
  }
)

Orders.create_order(
  %{
    address: "401 Main St",
    city: "Brooklyn",
    name: "Ted Lenny",
    pickup_date: NaiveDateTime.add(yesterday, -1 * 60 * 60, :second),
    state: "NY",
    zip: "11045",
    pharmacy_id: drugs_r_us_phar.id
  }
)

Orders.create_order(
  %{
    address: "410 Main St",
    city: "Brooklyn",
    name: "Ted Menny",
    pickup_date: yesterday,
    state: "NY",
    zip: "11045",
    pharmacy_id: drugs_r_us_phar.id
  }
)

Orders.create_order(
  %{
    address: "414 Main St",
    city: "New York City",
    name: "Ted Nenny",
    pickup_date: NaiveDateTime.add(yesterday, 1 * 60 * 60, :second),
    state: "NY",
    zip: "11011",
    pharmacy_id: drugs_r_us_phar.id
  }
)

Orders.create_order(
  %{
    address: "422 Main St",
    city: "Brooklyn",
    name: "Ted Oenny",
    pickup_date: NaiveDateTime.add(yesterday, 3 * 60 * 60, :second),
    state: "NY",
    zip: "11045",
    pharmacy_id: drugs_r_us_phar.id
  }
)

# Today
#Better Rx Pharmacy
Orders.create_order(
  %{
    address: "1 Main St",
    city: "New York City",
    name: "Red Aenny",
    pickup_date: NaiveDateTime.add(today, -3 * 60 * 60, :second),
    state: "NY",
    zip: "11011",
    pharmacy_id: better_rx.id
  }
)

Orders.create_order(
  %{
    address: "103 Main St",
    city: "Brooklyn",
    name: "Red Benny",
    pickup_date: NaiveDateTime.add(today, -1 * 60 * 60, :second),
    state: "NY",
    zip: "11045",
    pharmacy_id: better_rx.id
  }
)

Orders.create_order(
  %{
    address: "145 Main St",
    city: "Brooklyn",
    name: "Red Cenny",
    pickup_date: today,
    state: "NY",
    zip: "11045",
    pharmacy_id: better_rx.id
  }
)

Orders.create_order(
  %{
    address: "199 Main St",
    city: "New York City",
    name: "Red Denny",
    pickup_date: NaiveDateTime.add(today, 1 * 60 * 60, :second),
    state: "NY",
    zip: "11011",
    pharmacy_id: better_rx.id
  }
)

Orders.create_order(
  %{
    address: "204 Main St",
    city: "Brooklyn",
    name: "Red Eenny",
    pickup_date: NaiveDateTime.add(today, 3 * 60 * 60, :second),
    state: "NY",
    zip: "11045",
    pharmacy_id: better_rx.id
  }
)

#Best Rx Pharmacy
Orders.create_order(
  %{
    address: "222 Main St",
    city: "New York City",
    name: "Red Fenny",
    pickup_date: NaiveDateTime.add(today, -3 * 60 * 60, :second),
    state: "NY",
    zip: "11011",
    pharmacy_id: best_rx_phar.id
  }
)

Orders.create_order(
  %{
    address: "245 Main St",
    city: "Brooklyn",
    name: "Red Genny",
    pickup_date: NaiveDateTime.add(today, -1 * 60 * 60, :second),
    state: "NY",
    zip: "11045",
    pharmacy_id: best_rx_phar.id
  }
)

Orders.create_order(
  %{
    address: "277 Main St",
    city: "Brooklyn",
    name: "Red Henny",
    pickup_date: today,
    state: "NY",
    zip: "11045",
    pharmacy_id: best_rx_phar.id
  }
)

Orders.create_order(
  %{
    address: "299 Main St",
    city: "New York City",
    name: "Red Ienny",
    pickup_date: NaiveDateTime.add(today, 1 * 60 * 60, :second),
    state: "NY",
    zip: "11011",
    pharmacy_id: best_rx_phar.id
  }
)

Orders.create_order(
  %{
    address: "301 Main St",
    city: "Brooklyn",
    name: "Red Jenny",
    pickup_date: NaiveDateTime.add(today, 3 * 60 * 60, :second),
    state: "NY",
    zip: "11045",
    pharmacy_id: best_rx_phar.id
  }
)

#Drugs R Us Pharmacy
Orders.create_order(
  %{
    address: "399 Main St",
    city: "New York City",
    name: "Red Kenny",
    pickup_date: NaiveDateTime.add(today, -3 * 60 * 60, :second),
    state: "NY",
    zip: "11011",
    pharmacy_id: drugs_r_us_phar.id
  }
)

Orders.create_order(
  %{
    address: "401 Main St",
    city: "Brooklyn",
    name: "Red Lenny",
    pickup_date: NaiveDateTime.add(today, -1 * 60 * 60, :second),
    state: "NY",
    zip: "11045",
    pharmacy_id: drugs_r_us_phar.id
  }
)

Orders.create_order(
  %{
    address: "410 Main St",
    city: "Brooklyn",
    name: "Red Menny",
    pickup_date: today,
    state: "NY",
    zip: "11045",
    pharmacy_id: drugs_r_us_phar.id
  }
)

Orders.create_order(
  %{
    address: "414 Main St",
    city: "New York City",
    name: "Red Nenny",
    pickup_date: NaiveDateTime.add(today, 1 * 60 * 60, :second),
    state: "NY",
    zip: "11011",
    pharmacy_id: drugs_r_us_phar.id
  }
)

Orders.create_order(
  %{
    address: "422 Main St",
    city: "Brooklyn",
    name: "Red Oenny",
    pickup_date: NaiveDateTime.add(today, 3 * 60 * 60, :second),
    state: "NY",
    zip: "11045",
    pharmacy_id: drugs_r_us_phar.id
  }
)

# Tomorrow
#Better Rx Pharmacy
Orders.create_order(
  %{
    address: "1 Main St",
    city: "New York City",
    name: "Ned Aenny",
    pickup_date: NaiveDateTime.add(tomorrow, -3 * 60 * 60, :second),
    state: "NY",
    zip: "11011",
    pharmacy_id: better_rx.id
  }
)

Orders.create_order(
  %{
    address: "103 Main St",
    city: "Brooklyn",
    name: "Ned Benny",
    pickup_date: NaiveDateTime.add(tomorrow, -1 * 60 * 60, :second),
    state: "NY",
    zip: "11045",
    pharmacy_id: better_rx.id
  }
)

Orders.create_order(
  %{
    address: "145 Main St",
    city: "Brooklyn",
    name: "Ned Cenny",
    pickup_date: tomorrow,
    state: "NY",
    zip: "11045",
    pharmacy_id: better_rx.id
  }
)

Orders.create_order(
  %{
    address: "199 Main St",
    city: "New York City",
    name: "Ned Denny",
    pickup_date: NaiveDateTime.add(tomorrow, 1 * 60 * 60, :second),
    state: "NY",
    zip: "11011",
    pharmacy_id: better_rx.id
  }
)

Orders.create_order(
  %{
    address: "204 Main St",
    city: "Brooklyn",
    name: "Ned Eenny",
    pickup_date: NaiveDateTime.add(tomorrow, 3 * 60 * 60, :second),
    state: "NY",
    zip: "11045",
    pharmacy_id: better_rx.id
  }
)

#Best Rx Pharmacy
Orders.create_order(
  %{
    address: "222 Main St",
    city: "New York City",
    name: "Ned Fenny",
    pickup_date: NaiveDateTime.add(tomorrow, -3 * 60 * 60, :second),
    state: "NY",
    zip: "11011",
    pharmacy_id: best_rx_phar.id
  }
)

Orders.create_order(
  %{
    address: "245 Main St",
    city: "Brooklyn",
    name: "Ned Genny",
    pickup_date: NaiveDateTime.add(tomorrow, -1 * 60 * 60, :second),
    state: "NY",
    zip: "11045",
    pharmacy_id: best_rx_phar.id
  }
)

Orders.create_order(
  %{
    address: "277 Main St",
    city: "Brooklyn",
    name: "Ned Henny",
    pickup_date: tomorrow,
    state: "NY",
    zip: "11045",
    pharmacy_id: best_rx_phar.id
  }
)

Orders.create_order(
  %{
    address: "299 Main St",
    city: "New York City",
    name: "Ned Ienny",
    pickup_date: NaiveDateTime.add(tomorrow, 1 * 60 * 60, :second),
    state: "NY",
    zip: "11011",
    pharmacy_id: best_rx_phar.id
  }
)

Orders.create_order(
  %{
    address: "301 Main St",
    city: "Brooklyn",
    name: "Ned Jenny",
    pickup_date: NaiveDateTime.add(tomorrow, 3 * 60 * 60, :second),
    state: "NY",
    zip: "11045",
    pharmacy_id: best_rx_phar.id
  }
)

#Drugs R Us Pharmacy
Orders.create_order(
  %{
    address: "399 Main St",
    city: "New York City",
    name: "Ned Kenny",
    pickup_date: NaiveDateTime.add(tomorrow, -3 * 60 * 60, :second),
    state: "NY",
    zip: "11011",
    pharmacy_id: drugs_r_us_phar.id
  }
)

Orders.create_order(
  %{
    address: "401 Main St",
    city: "Brooklyn",
    name: "Ned Lenny",
    pickup_date: NaiveDateTime.add(tomorrow, -1 * 60 * 60, :second),
    state: "NY",
    zip: "11045",
    pharmacy_id: drugs_r_us_phar.id
  }
)

Orders.create_order(
  %{
    address: "410 Main St",
    city: "Brooklyn",
    name: "Ned Menny",
    pickup_date: tomorrow,
    state: "NY",
    zip: "11045",
    pharmacy_id: drugs_r_us_phar.id
  }
)

Orders.create_order(
  %{
    address: "414 Main St",
    city: "New York City",
    name: "Ned Nenny",
    pickup_date: NaiveDateTime.add(tomorrow, 1 * 60 * 60, :second),
    state: "NY",
    zip: "11011",
    pharmacy_id: drugs_r_us_phar.id
  }
)

Orders.create_order(
  %{
    address: "422 Main St",
    city: "Brooklyn",
    name: "Ned Oenny",
    pickup_date: NaiveDateTime.add(tomorrow, 3 * 60 * 60, :second),
    state: "NY",
    zip: "11045",
    pharmacy_id: drugs_r_us_phar.id
  }
)