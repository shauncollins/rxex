# RxEx

## Startup

Configure the database connections in dev.config and then run the following commands:

mix do deps.get, deps.compile
mix ecto.create
mix ecto.migrate
mix run priv/repo/seeds.exs
cd assets && npm install && node node_modules/webpack/bin/webpack.js --mode development

## Questions
### The hardest part of the implementation
The hardest part was figuring out how to map a courier user to the list of couriers and pharmacist to a list of pharmacies.  I didn't want the pharmacists to see the list of couriers and vice versa.  So I created a 2nd page in registration where the user picks the courier if they registred as a courier or a pharmacy if they registered as a pharmacist.

### What would be the next couple of tasks if I had more time
 * The seed data had many to one relationship between pharmacies and couriers but in the real world it would be many to many.  That's the biggest change I'd like to make.
 * Registering a courier and pharmacy has overlap that I'd like to condense and refactor to remove duplicate code.
 * Adding delivery date to old orders in the seed data.
 * When editing/deleting orders verify the user is a pharmacist and is a pharmacist that can edit/delete that order in the controller.
 
### How can we change the project to make it more interesting
 * If there was a repo we could fork with the basic framework and seed data of users that could log in, that would save some time in development where you could add more features to the project.

## Seed Data
1 Pharmacist for each pharmacy:

| Pharmacy | Username | Password |
|---|----|---|
| BetterRx | pharma@betterrx.com| password12345 |
| BestRx | pharma@bestrx.com | password12345 |
| DrugsRUs | pharma@drugsrus.com | password12345 |

1 Courier for each Courier Service

| Courier | Username | Password |
|---|---|---|
| Same Day Delivery | courier@samedaydelivery.com | password12345 |
| Previous Day Delivery | courier@previousdaydelivery.com | password12345 |

3 days of orders at all 3 pharmacies depending on when the seed script runs

These are the times used:
Yesterday and -3 hours
Yesterday and -1 hour
Yesterday
Yesterday and 1 hour
Yesterday and 3 hours

Today and -3 hours
Today and -1 hour
Today
Today and 1 hour
Today and 3 hours

Tomorrow and -3 hours
Tomorrow and -1 hour
Tomorrow
Tomorrow and 1 hour
Tomorrow and 3 hours

## Design Decisions
###Authentication
The phx.gen.auth task added way more than I thought it would.  It added settings controllers and session management, which is good but probably more than this project requires.  On top of the basic phx.gen.auth, I added role dropdown in the registration page.  This will let you decide if you are a pharmacist or a courier. The changes to do this were:
 * Updating Accounts.register_user -> Accounts.register_courier (the default)
 * Adding Accounts.register_pharmacist
 * Adding two new functions to User to register the two types of users
 * Adding a migration to the users table with the role
 * Updating the user model to hold the role
 * Adding the dropdown to user_registration/new.html.eex to allow the user to select their role when registering.
 * Adding an uneditable text input to user_settings/edit.html.eex to show the selected role but not allow it to change.
 * Updating unit tests to default users to courier and added new tests for pharmacists

### Controllers/Contexts/Views
I added a Context: Address to manage Pharmacy and Courier addresses.  I removed the ability to delete them.

Added many pharmacies to 1 courier.  Based on the seed data that looks correct but a more likely scenario is it is Many To Many.

After registering a new user as a pharmacist or a courier, the user is then asked to pick the courier or pharmacy they are part of.
This is probably a mistake I made reading the requirements where I thought users belonged to the pharmacies or courier systems instead of the pharmacy and courier service being the user.

Added orders to the seed data and Updated the default index endpoint to only list orders for the current user and with a pickup date of today.

For couriers, I had to change the load query to preload the pharmacy so the index screen can tell them where to pick up the order.

Gave pharmacists the ability to add, delete, edit orders.  

Gave couriers the ability to mark orders delivered.

# Things I couldn't figure out (just being honest)
## Testing
When testing the orders controller I couldn't figure out how to change the log in fixture to add a pharmacy_id to the logged in user.  
