# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :rxex,
  namespace: RxEx,
  ecto_repos: [RxEx.Repo]

# Configures the endpoint
config :rxex, RxExWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "YZgGx+nUNrG8fHuZAUE+oooLOI2d+xC6zynges7Xe6t0ZTMx8lhUCsstfxeqEWmT",
  render_errors: [view: RxExWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: RxEx.PubSub,
  live_view: [signing_salt: "RwFeL/eQ"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
